<?php
function cek_pesan($teks)
{
    $kata = array("penyakit", "makan", "makanan", "minum","minuman", "asupan", "sakit", "gula", "darah", "penderita", "kesehatan");
    $hasil = 0;
    $jml_kata = count($kata);
    for ($i = 0; $i < $jml_kata; $i++) {
        if (stristr($teks, $kata[$i])) {
            $hasil = "Positif";
            break;
        } else {
            $hasil = "Negatif";
        }
    }
    return $hasil;
}
?>
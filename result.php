<?php
include "connection.php";

$tables = array('jakartapusat', 'jakartautara', 'jakartaselatan', 'jakartabarat', 'jakartatimur', 'kepSeribu');

foreach ($tables as $table) {
    $sql = "SELECT COUNT(*) AS total FROM $table";
    $data = mysqli_fetch_assoc(mysqli_query($conn, $sql));
    $total = $data['total'];
    echo "Total tweet $table : " . $total . "<br>";

    $sql = "SELECT COUNT(*) AS positif FROM $table WHERE stat = 'positif'";
    $data = mysqli_fetch_assoc(mysqli_query($conn, $sql));
    $total = $data['positif'];
    echo "Total tweet positif $table : " . $total . "<br>";

    $sql = "SELECT COUNT(*) AS negatif FROM $table WHERE stat = 'negatif'";
    $data = mysqli_fetch_assoc(mysqli_query($conn, $sql));
    $total = $data['negatif'];
    echo "Total tweet negatif $table : " . $total. "<br>";
    echo "<hr>";
}